package main

import (
	"flag"
	"log"
	"net"
	"strconv"
)

type IAddrLookup map[string](*net.UDPAddr)

type IMsg struct {
	msg   string
	raddr *net.UDPAddr
}

func main() {
	port := flag.Int("port", 50000, "server port")
	flag.Parse()
	run(*port)
}

func sendout(ch chan IMsg, conn *net.UDPConn) {
	var msg IMsg
	lookup := make(IAddrLookup)
	for {
		msg = <-ch
		if lookup[msg.raddr.String()] == nil {
			lookup[msg.raddr.String()] = msg.raddr
		}
		for raddrStr, raddr := range lookup {
			if raddrStr != msg.raddr.String() {
				conn.WriteToUDP([]byte(raddr.String()+"\t"+msg.msg), raddr)
			}
		}
	}
}

func run(port int) error {
	/*
	 * init UDP listener on :50000
	 */
	laddr, err := net.ResolveUDPAddr("udp", ":"+strconv.Itoa(port))
	if err != nil {
		log.Println("some kind of addressing problem.. ", err)
		return err
	}
	conn, err := net.ListenUDP("udp", laddr)
	if err != nil {
		log.Println("some kind of listening problem.. ", err)
		return err
	}
	defer conn.Close()
	/*
	 * spin off sendout
	 */
	sendoutCh := make(chan IMsg)
	go sendout(sendoutCh, conn)
	/*
	 * init recv buffer
	 */
	buf := make([]byte, 2048)
	oob := make([]byte, 2048)
	for {
		n, oobn, _, raddr, err := conn.ReadMsgUDP(buf, oob)
		log.Println(n, oobn, raddr, string(buf))
		if err != nil {
			log.Println(err)
		}
		sendoutCh <- IMsg{msg: string(buf[:n]), raddr: raddr}
	}
}
