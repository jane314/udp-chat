# UDP Chat

UDP Chat! Move your group chat to the transport layer. 🙂

Modern group chat systems like iMessage and Facebook Messenger implement
countless features that use a lot of data together: names, avatar images, 💙
reactions, read receipts, and more.

UDP Chat helps you talk to your friend group with pure UDP datagrams. Nothing is
layered on top of your raw datagrams in a UDP Chat except your hostname and
port, which identify you to your friends. With UDP chat, you get to learn your
friends' IP addresses!

# Setup

Server 

```bash
go build`
./udpchat --port 50000
```

Client
```bash
nc -uv $server_hostname 50000
```

# Screenshots

Server

<img src="pics/1.png" width="500"/>

Client 1

<img src="pics/2.png" width="500"/>

Client 2

<img src="pics/3.png" width="500"/>


